package com.velykyi.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;

import java.io.File;
import java.io.IOException;

public class JSONValidator {

    public static boolean validate(File json, File jsonSchema) {
        try {
            final JsonNode data = JsonLoader.fromFile(json);
            final JsonNode schema = JsonLoader.fromFile(jsonSchema);
            final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
            JsonValidator validator = factory.getValidator();
            ProcessingReport report = validator.validate(schema, data);
            System.out.println(report.isSuccess());
            return report.isSuccess();
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }
        return false;
    }
}
