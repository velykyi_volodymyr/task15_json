package com.velykyi.model;

public class Value {
    private double proteins;
    private double fat;
    private double carbohydrates;

    public Value() {
    }

    public Value(double proteins, double fat, double carbohydrates) {
        this.proteins = proteins;
        this.fat = fat;
        this.carbohydrates = carbohydrates;
    }

    public double getProteins() {
        return proteins;
    }

    public void setProteins(double proteins) {
        this.proteins = proteins;
    }

    public double getFat() {
        return fat;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(double carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    @Override
    public String toString() {
        return "Value{" +
                "proteins=" + proteins +
                ", fat=" + fat +
                ", carbohydrates=" + carbohydrates +
                '}';
    }
}
