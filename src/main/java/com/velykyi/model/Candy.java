package com.velykyi.model;

import java.util.List;

public class Candy implements Comparable<Candy> {
    private String name;
    private double energy;
    private String type;
    private List<String> ingredients;
    private Value value;
    private String production;

    public Candy() {
    }

    public Candy(String name, double energy, String type, List<String> ingredients, Value values, String production) {
        this.name = name;
        this.energy = energy;
        this.type = type;
        this.ingredients = ingredients;
        this.value = values;
        this.production = production;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value values) {
        this.value = values;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    @Override
    public String toString() {
        return "Candy{" +
                "name='" + name + '\'' +
                ", energy=" + energy +
                ", type='" + type + '\'' +
                ", ingredients=" + ingredients +
                ", values=" + value +
                ", production='" + production + '\'' +
                '}';
    }

    @Override
    public int compareTo(Candy otherCandy) {
        return name.compareTo(otherCandy.getName());
    }
}
