package com.velykyi.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.velykyi.model.Candy;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JacksonParser {
     private ObjectMapper objectMapper;

    public JacksonParser() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Candy> getList(File json) throws IOException {
        Candy[] candies = objectMapper.readValue(json, Candy[].class);
        return Arrays.asList(candies);
    }
}
