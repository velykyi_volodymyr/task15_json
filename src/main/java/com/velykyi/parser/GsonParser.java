package com.velykyi.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.velykyi.model.Candy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class GsonParser {
    private Gson gson;
    private static Logger logger1 = LogManager.getLogger(GsonParser.class);

    public GsonParser() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    public List<Candy> getList(File json) throws FileNotFoundException {
        Candy[] candies = gson.fromJson(new FileReader(json), Candy[].class);
        return Arrays.asList(candies);
    }

    public void writeList(List<Candy> candies, File newJson) {
        try (Writer writer = new FileWriter(newJson)) {
            gson.toJson(candies, writer);
        } catch (Exception ignored) {
            logger1.error("Something wrong with file.");
        }
        logger1.info("Json was written to file " + newJson);
    }
}
