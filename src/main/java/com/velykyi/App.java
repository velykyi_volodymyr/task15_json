package com.velykyi;

import com.velykyi.model.Candy;
import com.velykyi.parser.GsonParser;
import com.velykyi.parser.JacksonParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {
    private static Logger logger1 = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        File data = new File("D:\\Epam Java Course\\Topic 18. JSON\\task15_json\\src\\main\\resources\\candy.json");
        File schema = new File("D:\\Epam Java Course\\Topic 18. JSON\\task15_json\\src\\main\\resources\\candySchema.json");
//        System.out.println(JSONValidator.validate(data, schema));
        GsonParser gparser = new GsonParser();
        JacksonParser jparser = new JacksonParser();
        List<Candy> gsonList = new ArrayList<>();
        List<Candy> jacksonList = new ArrayList<>();
        try{
            gsonList = gparser.getList(data);
            jacksonList = jparser.getList(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Collections.sort(jacksonList);
        logger1.info("Gson parser:");
        printList(gsonList);
        logger1.info("Jackson parser:");
        printList(jacksonList);
        Collections.sort(gsonList);
        gparser.writeList(gsonList, new File("GsonSort.json"));
    }

    private static void printList(List<Candy> candies) {
        for (Candy candy : candies) {
            logger1.info(candy);
        }
    }
}
